const { distribute_money_fx, qn1 } = require('./Distribute_money_randomly.ts')
// npm i --save-dev @types/jest


test('sum of random input should match input money', () => {
    let money_array = distribute_money_fx(10.5, 5)
    let total_sum = parseFloat(money_array.reduce((partialSum, a) => partialSum + a, 0).toFixed(2))
    expect(total_sum).toBe(10.5)
    expect(total_sum).not.toBe(11)


    money_array = distribute_money_fx(1.11, 3)
    total_sum = parseFloat(money_array.reduce((partialSum, a) => partialSum + a, 0).toFixed(2))
    expect(total_sum).toBe(1.1)


    money_array = distribute_money_fx(1, 1)
    total_sum = parseFloat(money_array.reduce((partialSum, a) => partialSum + a, 0).toFixed(2))
    expect(total_sum).toBe(1)

    money_array = distribute_money_fx(0, 0)
    total_sum = parseFloat(money_array.reduce((partialSum, a) => partialSum + a, 0).toFixed(2))
    expect(total_sum).toBe(0)
})
  
////////////////////////////////////////////////////

test('all values at least 0.1', () => {
    let money_array = distribute_money_fx(0.1, 2)
    console.log(money_array)
    expect( money_array.includes(0) ).toBe(true)


    // money_array = distribute_money_fx(0.3, 6)
    // console.log(money_array)
    // expect( money_array.includes(0) ).toBe(true)

    // money_array = distribute_money_fx(0.3, 6)
    // console.log(money_array)
    // expect( money_array.includes(0) ).toBe(false)

    // money_array = distribute_money_fx(0.1, 1)
    // console.log(money_array)
    // expect( money_array.includes(0) ).toBe(false)

    // money_array = distribute_money_fx(0.1, 0)
    // console.log(money_array)
    // expect( money_array.includes(0) ).toBe(false)

})

 
